namespace PrisonerDilemma;
class Prisoner2039039 : IPrisoner
{
    private static Random Random { get; } = new Random();
    public string Name { get; } = "Nick2039039";
    private int defectTime=0;
    private int coopTime=0;


    public PrisonerChoice GetChoice()
    {
      if (defectTime>3){
        return PrisonerChoice.Defect;

      }
      if(coopTime>3){
         return PrisonerChoice.Defect;
      }
        return Random.Next(100) < 52 ?
      PrisonerChoice.Cooperate : PrisonerChoice.Defect;
    }

    public void ReceiveOtherChoice(PrisonerChoice otherChoice)
    {//this is used to see if they are just spaming one choice to counter it workes best when multiple games are played
      if (otherChoice==PrisonerChoice.Defect){
        defectTime++;
        coopTime=0;
      }
      if (otherChoice==PrisonerChoice.Cooperate){
        coopTime++;
        defectTime=0;
      }

    }

    public void Reset(DilemmaParameters dilemmaParameters)
    {
      //reset counter for next prisoner
      defectTime=0;
      coopTime=0;
    }
    
}

