﻿using System.Collections.Immutable;
using System.Reflection;
using PrisonerDilemma;

// TODO : No namespace directive is used here, what happens then?

// TODO : What access modifier is used when none is explicitely written?
class Program
{
  // TODO : What is the advantage of using const?
  const int MaxPrisonerNameLength = 50;

  static void Main()
  {
    // TODO : What does it mean for a data type to be immutable?
    ImmutableArray<IPrisoner> prisoners = GetPrisoners();

    Console.Write("Enter the number of iterations: ");
    // TODO : What is that `??` operator?
    // TODO : I can call methods on a primitive type?!
    int numIterations = int.Parse(Console.ReadLine() ?? "0");
    Console.WriteLine();

    // TODO : Why can/should I use the `var` keyword here?
    var prisonerTournament = new PrisonerTournament(prisoners, numIterations);

    double[] scores = prisonerTournament.Start();

    PrintTournamentResult(prisoners, scores);
  }

  /// <summary>
  /// Pretty-prints the results of a tournament, where prisoners are ordered by
  /// their scores.
  /// </summary>
  static void PrintTournamentResult(
    ImmutableArray<IPrisoner> prisoners, double[] scores
    )
  {
    int positionNumDigits = (int)Math.Floor(Math.Log10(prisoners.Length) + 1);
    int longestNameLength = Math.Min(
      prisoners.Max(prisoner => prisoner.Name.Length), MaxPrisonerNameLength
      );

    int position = 0;
    foreach ((IPrisoner prisoner, double score) in prisoners
      // No need to understand what Zip() does for now, we'll cover that later.
      .Zip(scores)
      // TODO : What is that `=>` syntax, and what does it accomplish here?
      // TODO : What does the generic types of OrderBy each mean?
      .OrderBy<(IPrisoner _, double score), double>(t => t.score)
      .Reverse())
    {
      position++;

      string paddedPosition = position.ToString()
        .PadLeft(positionNumDigits, '0');
      string paddedPrisonerName = prisoner.Name.Length > MaxPrisonerNameLength ?
        prisoner.Name.Substring(0, MaxPrisonerNameLength) :
        prisoner.Name.PadRight(longestNameLength);

      Console.WriteLine(
        $"#{paddedPosition}. {paddedPrisonerName} scored {score}"
        );
    }
  }

  /// <summary>
  /// Gets instances of classes implementing IPrisoner, in the PrisonerDilemma
  /// namespace.
  /// </summary>
  static ImmutableArray<IPrisoner> GetPrisoners()
  {
    // No need to understand this entire method.

    // But, if you are interested, this uses a concept called 'reflection'
    return Assembly
      .GetExecutingAssembly()
      .GetTypes()
      // Need to restrict to IsClass, as interfaces could implement IPrisoner.
      .Where(type => type.IsClass && type.Namespace == "PrisonerDilemma" &&
        type.IsAssignableTo(typeof(IPrisoner)))
      // Map each type, to an instance of that type, by calling its constructor.
      .Select((type, _) => type
        .GetConstructor(Array.Empty<Type>())
        !.Invoke(Array.Empty<Type>()))
      .Cast<IPrisoner>()
      .ToImmutableArray();
  }
}