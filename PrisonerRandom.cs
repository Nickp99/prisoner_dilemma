namespace PrisonerDilemma;

/// <summary>
/// Defects of cooperates with equal probability
/// </summary>
class PrisonerRandom : IPrisoner
{
  private static Random Random { get; } = new Random();

  public string Name { get; } = "Random";

  public void Reset(DilemmaParameters dilemmaParameters) { }

  public PrisonerChoice GetChoice()
  {
    return Random.Next(2) < 1 ?
      PrisonerChoice.Cooperate : PrisonerChoice.Defect;
  }

  public void ReceiveOtherChoice(PrisonerChoice otherChoice) { }
}