# Lab 00 - Prisoner's Dilemma

## Summary

Read on [Prisoner's Dilemma](https://en.wikipedia.org/wiki/Prisoner's_dilemma).

My goal for this lab is for you to get a feel of the C# language. You'll do this by first understanding the code provided and then coding your own prisoner "AI".

I will then take all of your prisoners (that sounds ominous...) and make them compete in a round-robin tournament of iterated prisoner's dilemmas.

## Instructions

1. Fork this repository
2. Create a C# source code file named `Prisoner<your-student-id>.cs` (e.g. `Prisoner1633477.cs`) in the top-level of the repository.
3. In that file, create a class with the same name, inside the namespace `PrisonerDilemma`. This class must implement the `IPrisoner` interface.
4. Once you're happy with your prisoner AI, create a merge request to the original repository (so that I can access your code).

While you are reading my code, notice the questions in the various TODO questions. No need to answer all of them, but note down those that you do answer (keep the links where you found the info in a document somewhere).

## Grading

This is not graded, but I'd like all the prisoner AIs to be submitted by the end of the day, so I can run them before next class (where I will present the results!)

What should the winner(s) of this tournament get as a reward...? (give suggestions if you have any)
